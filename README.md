<img align="left" src="assets/logo.png">

### Hey - Salam Alaikum!
*I'm a person who's obsessed about **Linux** and loves to write `<code>`*

<br/><br/>



Hi, I'm a [***freelancer***](https://fiverr.com/abdulrahman1s) at Fiverr.com.
I mostly write **Rust** & **JS/TS** these days, <br/> 
additionally I have a solid experience with **Go**, **Python** and **Dart** <br/>
and yeah I am [***the fourth most active github user in egypt.***](https://committers.top/egypt) 


<details>
<summary>📈 <b>Statistics</b></summary>

![statistics](assets/statistics.svg)

</details>

<details>
<summary>✨ <b>Achievements</b></summary>

![achievements](assets/achievements.svg)
</details>

<details>
<summary>👀 <b>Recent articles</b></summary>

[![articles](assets/articles.svg)](https://dev.to/abdulrahman1s)
</details>


<details>
<summary>😍 <b>Feedback</b></summary>

<!--feedback_start-->
- [@t_t_a_m](https://fiverr.com/t_t_a_m): **Very helpfull and very fast working!**
- [@jork888](https://fiverr.com/jork888): **an understanding person. Clever**
<!--feedback_end-->

[![feedback_button](https://img.shields.io/badge/submit%20feedback-30363D?style=for-the-badge&logo=GitHub-Sponsors&logoColor=#white)](https://github.com/abdulrahman1s/abdulrahman1s/issues/new?assignees=&labels=feedback&template=feedback.yml&title=Feedback+Submission)

> Source: [fiverr](https://fiverr.com/abdulrahman1s) & [github](https://github.com/abdulrahman1s/abdulrahman1s/issues?q=is%3Aissue+label%3Afeedback)
</details>


### 💬 Follow me
- <img height="16" width="16" src="assets/icons/twitter.png" /> [@abdulrahman1s_](https://twitter.com/TheMaestro1s) at Twitter
- <img height="16" width="16" src="assets/icons/linkedin.png" /> [@abdulrahmann](https://linkedin.com/in/abdulrahmann) at Linkedin
- <img height="16" width="16" src="assets/icons/fiverr.png" /> [@abdulrahman1s](https://fiverr.com/abdulrahman1s) at Fiverr
- <img height="16" width="16" src="assets/icons/devto.png" /> [@abdulrahman1s](https://dev.to/abdulrahman1s) at Dev.to

<div align="center">
    Show some <a href="https://quran.com/en/saba/39">❤️</a> by <a href="https://ko-fi.com/abdulrahman1s">Buying me a Coffee ☕</a>
</div>
